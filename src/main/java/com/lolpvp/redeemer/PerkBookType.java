package com.lolpvp.redeemer;

enum PerkBookType
{
	PERMISSION,
	MULTIPLE_PERMISSIONS,
	GROUP;
}