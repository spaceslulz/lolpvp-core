package com.lolpvp.core;

public class Permissions {
    private static final String PREFIX = "lolpvp.";

    //Votes
    public static final String VOTES = PREFIX + "votes",
            VOTES_OTHERS = VOTES + ".others",
            VOTES_TOP = VOTES + ".top",
            VOTES_RESET = VOTES + ".reset";

    public static final String SIGNS = PREFIX + "signs",
            SIGNS_CREATE = SIGNS + ".create",
            SIGNS_DESTROY = SIGNS + ".destroy",
            SIGNS_ADD_COMMAND = SIGNS + "add-command";


}
